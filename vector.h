#pragma once

#include <cmath>

// forward declaration since we need vec in math_helper
template<unsigned int dimension>
struct vec;

using vec2 = vec<2>;
using vec3 = vec<3>;
using vec4 = vec<4>;

#include "math_helper.h"

template<unsigned int dimension>
struct vec {

    float data[dimension];

    template<typename ... T>
    vec(T ... values) : data{values ...} {};

    vec(const vec<dimension> &v) {
        set_to(v);
    }

    vec() : data{0} {}

    float &operator[](unsigned int index) { return data[index]; }
    float operator[](unsigned int index) const { return data[index]; }

    vec<dimension> operator+(const vec<dimension> &v) const {
        vec<dimension> result;
        for (unsigned int i = 0; i < dimension; i++) {
            result[i] = data[i] + v[i];
        }

        return result;
    }

    vec<dimension> operator-(const vec<dimension> &v) const {
        vec<dimension> result;
        for (unsigned int i = 0; i < dimension; i++) {
            result[i] = data[i] - v[i];
        }

        return result;
    }

    vec<dimension> operator*(float scale) const {
        vec<dimension> result;
        for (unsigned int i = 0; i < dimension; i++) {
            result[i] = data[i] * scale;
        }

        return result;
    }

    vec<dimension> operator/(float scale) const {
        return (*this) * (1.0f / scale);
    }

    bool operator==(const vec<dimension> &v) const {
        for (unsigned int i = 0; i < dimension; i++) {
            if (!nearly_equal(data[i], v[i]))
                return false;
        }

        return true;
    }

    bool operator!=(const vec<dimension> &v) const {
        return !(*this == v);
    }

    float dot(const vec<dimension> &v) const {
        float result = 0;
        for (unsigned int i = 0; i < dimension; i++) {
            result += v[i] * data[i];
        }
        return result;
    }

    float angle(const vec<dimension> &v) const {
        return acos(this->dot(v) / (v.len() * this->len()));
    }

    vec<dimension> project_onto(const vec<dimension> &v) const {
        return v * (this->dot(v) / v.dot(v));
    }

    float len() const {
        return sqrt(dot(*this));
    }

    void set_to(const vec<dimension> &v) {
        for (unsigned int i = 0; i < dimension; i++) {
            data[i] = v[i];
        }
    }

    void norm() {
        vec<dimension> result = (*this) / len();
        set_to(result);
    }

    void neg() {
        vec<dimension> result = *this * (-1);
    }

    vec<dimension> get_ortho_vec() {
        vec<dimension> result;
        result[0] = -data[1];
        result[1] = data[0];

        return result;
    }

    static float cross(const vec<2> v, const vec<2> v2) {
        return v[0] * v2[1] - v[1] * v2[0];
    }

    static vec<3> cross(const vec<3> v, const vec<3> v2) {
        vec<3> result;

        result[0] = v[1] * v2[2] - v[2] * v2[1];
        result[1] = v[2] * v2[0] - v[0] * v2[2];
        result[2] = v[0] * v2[1] - v[1] * v2[0];

        return result;
    }

};
