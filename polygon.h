#pragma once

#include <cmath>
#include <algorithm>
#include <vector>
#include <limits>

#include "vector.h"

template<unsigned int dimension>
struct polygon {

    using size_type = std::vector<vec2>::size_type;

    struct projection {
        float min, max;
    };

    polygon(std::vector<vec<dimension>> vertices)
        : vertices(vertices), normals(vertices.size()) {
        invalidate_all_normals();
        recalculate_normals();
    }

    polygon(const polygon &p) : polygon(p.vertices) { }

    bool collides(const polygon<dimension> &p) const {
        vec2 tmp;
        return collides(p, tmp);
    }

    bool collides(const polygon<dimension> &p, vec2 &out) const {
        vec2 smallest_axis;
        float scale = std::numeric_limits<float>::max();

        recalculate_normals();
        p.recalculate_normals();

        auto &poly1 = *this;
        auto &poly2 = p;

        auto seperating_axis_theorem =
            [&poly1, &poly2, &scale, &smallest_axis]
            (const std::pair<bool, vec<2>> &normal) -> bool {
                projection p1 = poly1.project_onto_vec(normal.second);
                projection p2 = poly2.project_onto_vec(normal.second);

                if (!overlap(p1.min, p1.max, p2.min, p2.max)) {
                    return true;
                } else {
                    float overlap = std::min(p1.max, p2.max) - std::max(p1.min, p2.min);

                    if (overlap < scale) {
                        scale = overlap;
                        smallest_axis = normal.second;
                    }

                    return false;
                }
        };

        bool seperated = std::any_of(normals.cbegin(), normals.cend(), seperating_axis_theorem);
        seperated |= std::any_of(p.normals.cbegin(), p.normals.cend(), seperating_axis_theorem);

        out = smallest_axis * scale;

        return !seperated;

    }

    projection project_onto_vec(const vec<dimension> &v) const {
        projection result;
        float &min = result.min;
        float &max = result.max;

        min = v.dot(vertices[0]);
        max = min;

        for (vec<dimension> e : vertices) {
            float projection = e.dot(v);

            min = min < projection ? min :projection;
            max = max > projection ? max : projection;
        }
        return result;
    }

    const vec<dimension> &operator[](unsigned int index) const {
        return vertices[index];
    }

    const vec<dimension> get_normal(unsigned int index) const {
        if (!normals[index].first)
            recalculate_normal(index);
        return normals[index].second;
    }

    vec<2> get_point_in_polygon() {
        vec<2> point;

        for (size_type i = 0; i < vertices.size(); i++) {
            point = point + vertices[i];
        }

        return point * (1.0f / vertices.size());
    }

    void set_vector(unsigned int index, vec<dimension> new_val) {
        int prev_index = (index > 0) ? (index - 1) : (normals.size() - 1);
        normals[index].first = normals[prev_index].first = false;
        vertices[index] = new_val;
    }

    bool operator==(const polygon &p) const {
        return vertices == p.vertices;
    }

    bool operator!=(const polygon &p) const {
        return !(*this == p);
    }

    void translate(vec<dimension> translation) {
        for (vec<dimension> &v : vertices) {
            v = v + translation;
        }
    }

    int number_of_vertices() const {
        return vertices.size();
    }

    private :

    void reorder_all_vertices() {
        invalidate_all_normals();

        vec2 translation = get_point_in_polygon();
        translate(translation * -1);

        std::sort(vertices.begin(), vertices.end(), [](vec2 p1, vec2 p2) {
            return atan2(p1[0], p1[1]) > atan2(p2[0], p2[1]);
        });

        translate(translation);
        recalculate_normals();
    }

    void invalidate_all_normals() const {
        for (int i = 0; i < normals.size(); i++)
            normals[i].first = false;
    }

    void recalculate_normal(unsigned int index) const {
        // normal is valid there's no need to recalculate it
        if (normals[index].first)
            return;

        int next = (index + 1) % vertices.size();

        vec2 normal = (vertices[index] - vertices[next]).get_ortho_vec();
        normal.norm();

        normals[index].first = true;
        normals[index].second = normal;
    }

    void recalculate_normals() const {
        for (std::vector<int>::size_type i = 0; i < normals.size(); i++) {
            if (!normals[i].first)
                recalculate_normal(i);
        }
    }

    mutable std::vector<std::pair<bool, vec<dimension>>> normals;
    std::vector<vec<dimension>> vertices;
};
