#include <cmath>
#include <limits>

#include "math_helper.h"

bool nearly_equal(float v, float v2) {
    static float epsilon = std::numeric_limits<float>::epsilon();
    return fabs(v - v2) <= epsilon;
}

bool overlap(float min1, float max1, float min2, float max2) {
    return !(min2 >= max1 || min1 >= max2);
}

vec2 intersection_lines(vec2 p1, vec2 p2, vec2 p3, vec2 p4) {
    vec2 intersection;
    float den = 1.0f / vec2::cross(p1 - p2, p3 - p4);
    intersection[0] = (vec2::cross(p1, p2) * (p3[0] - p4[0]) - (p1[0] - p2[0]) * vec2::cross(p3, p4)) * den;
    intersection[1] = (vec2::cross(p1, p2) * (p3[1] - p4[1]) - (p1[1] - p2[1]) * vec2::cross(p3, p4)) * den;
    return intersection;
}
