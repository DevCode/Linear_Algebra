#pragma once

#include "vector.h"

bool nearly_equal(float v, float v2);
bool overlap(float min1, float max1, float min2, float max2);
vec2 intersection_lines(vec2 l1p1, vec2 l1p2, vec2 l2p1, vec2 l2p2);
